
PROGRAM _INIT
	(* Insert code here *)
	gCoTrace.Out.Status.Ready := TRUE;
	gCoTrace.Inp.Params.NumOfParams := 1;
	gCoTrace.Inp.Params.TimeRecording := 250;
	
	gCoTrace.Inp.Params.NumOfParams   :=  6;
	gCoTrace.Inp.Params.ParamsName[0] := 'gAxis.Out.Status.ActVelocity';			
	gCoTrace.Inp.Params.ParamsName[1] := 'gAxis.Out.Status.ActTorque';
	gCoTrace.Inp.Params.ParamsName[2] := 'gAxis.Out.Status.ActPower';
	gCoTrace.Inp.Params.ParamsName[3] := 'gAxis.Out.Status.SetIq';
	gCoTrace.Inp.Params.ParamsName[4] := 'gAxis.Out.Status.Uq';
	gCoTrace.Inp.Params.ParamsName[5] := 'gAxis.Out.Status.Ud';

	
END_PROGRAM

PROGRAM _CYCLIC
	
	CASE gCoTrace.Out.State.SM OF
		CT_WAIT_FOR_CMD:
		
			IF gCoTrace.Inp.Cmd.Start THEN
				gCoTrace.Inp.Cmd.Start := FALSE;
				gCoTrace.Out.Status.Done := FALSE;
				gCoTrace.Out.Status.Ready := FALSE;
				
				CoTraceInter.CoTraceCreate.Enable := FALSE;
				gCoTrace.Out.State.SM := CT_CONFIG_CREATE;
				
			ELSIF gCoTrace.Inp.Cmd.Stop THEN
				gCoTrace.Inp.Cmd.Stop := FALSE;	
			
			END_IF
		
		CT_CONFIG_CREATE:
			CoTraceInter.CoTraceCreate.Enable := TRUE;
			
			IF CoTraceInter.CoTraceCreate.Active THEN
				gCoTrace.Out.State.SM := CT_SET_TIMING;
				
			ELSIF CoTraceInter.CoTraceCreate.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;
			END_IF
			
		
		CT_SET_TIMING:
			CoTraceInter.CoTraceSetTiming.TotalRecordingTime := gCoTrace.Inp.Params.TimeRecording;
			CoTraceInter.CoTraceSetTiming.TriggerOffsetTime := 0;
			CoTraceInter.CoTraceSetTiming.ConfigIdent := CoTraceInter.CoTraceCreate.ConfigIdent;
			CoTraceInter.CoTraceSetTiming.Execute := TRUE;
			
			CoTraceInter.CoTraceSetTiming();
			IF CoTraceInter.CoTraceSetTiming.Done THEN
				CoTraceInter.CoTraceSetTiming.Execute := FALSE;
				CoTraceInter.CoTraceSetTiming();

				i := 0;
				gCoTrace.Out.State.SM := CT_ADD_DATA_POINT;
				
			ELSIF CoTraceInter.CoTraceSetTiming.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;
			END_IF
		
		CT_ADD_DATA_POINT:
							
			CoTraceInter.CoTraceAddDataPoint.ConfigIdent   := CoTraceInter.CoTraceCreate.ConfigIdent;
			CoTraceInter.CoTraceAddDataPoint.DataPointName := gCoTrace.Inp.Params.ParamsName[i];
			CoTraceInter.CoTraceAddDataPoint.Execute 	   := TRUE;
			CoTraceInter.CoTraceAddDataPoint();
								
			
			IF CoTraceInter.CoTraceAddDataPoint.Done THEN
				CoTraceInter.CoTraceAddDataPoint.Execute := FALSE;
				CoTraceInter.CoTraceAddDataPoint();
				
				IF i = gCoTrace.Inp.Params.NumOfParams - 1 THEN
					gCoTrace.Out.State.SM := CT_SET_TRIGGER;
				ELSE
					i := i + 1;		
				END_IF				
				
			ELSIF CoTraceInter.CoTraceAddDataPoint.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;	
			END_IF
			
			
		
		CT_SET_TRIGGER:
			CoTraceInter.CoTraceAddStartTrigger.ConfigIdent   := CoTraceInter.CoTraceCreate.ConfigIdent;
			CoTraceInter.CoTraceAddStartTrigger.DataPointName := 'gAxis.Out.Status.ActVelocity';
			CoTraceInter.CoTraceAddStartTrigger.Condition := coTRACE_ABOVE_THRESHOLD_EVENT; 
			CoTraceInter.CoTraceAddStartTrigger.Threshold := 0;
			CoTraceInter.CoTraceAddStartTrigger.Window    := 0;
			CoTraceInter.CoTraceAddStartTrigger.Execute   := TRUE;
			
			CoTraceInter.CoTraceAddStartTrigger();
			IF CoTraceInter.CoTraceAddStartTrigger.Done THEN
				CoTraceInter.CoTraceAddStartTrigger.Execute := FALSE;
				CoTraceInter.CoTraceAddStartTrigger();
				gCoTrace.Out.State.SM := CT_OPTIONAL;
				
			ELSIF CoTraceInter.CoTraceAddStartTrigger.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;
				
			END_IF
			
			
		CT_OPTIONAL:
			CoTraceInter.CoTraceOptional.Execute := TRUE;
			CoTraceInter.CoTraceOptional.ConfigIdent := CoTraceInter.CoTraceCreate.ConfigIdent;
			CoTraceInter.CoTraceOptional.Parameter   := coTRACE_PARAM_PLC_SAMPLE_TIME;
			CoTraceInter.CoTraceOptional.Value		 := 0.5;
			CoTraceInter.CoTraceOptional();
			
			IF CoTraceInter.CoTraceOptional.Done THEN
				CoTraceInter.CoTraceAddStartTrigger.Execute := FALSE;
				CoTraceInter.CoTraceAddStartTrigger();
				
				gCoTrace.Out.State.SM := CT_RECORD;
				
			ELSIF CoTraceInter.CoTraceOptional.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;
				
			END_IF
			
		CT_RECORD:
			CoTraceInter.CoTraceRecorder.ConfigIdent := CoTraceInter.CoTraceCreate.ConfigIdent;
			CoTraceInter.CoTraceRecorder.Enable 	 := TRUE;
			CoTraceInter.CoTraceRecorder();
		
			IF CoTraceInter.CoTraceRecorder.DataAvailable THEN
				CoTraceInter.CoTraceRecorder.Enable  := FALSE;
				
				gCoTrace.Out.State.SM := CT_SAVE_CSV;
				
			ELSIF CoTraceInter.CoTraceRecorder.Error THEN
				gCoTrace.Out.State.SM := CT_ERROR;
				
			END_IF
		
			
			IF gCoTrace.Inp.Cmd.Stop THEN
				gCoTrace.Inp.Cmd.Stop := FALSE;
				CoTraceInter.CoTraceRecorder.Enable := FALSE;
				CoTraceInter.CoTraceRecorder();
				gCoTrace.Out.State.SM := CT_DONE;
			END_IF
				
				
				
		CT_SAVE_CSV:
				CoTraceInter.CoTraceSaveCsv.ColumnSeparator := ';';
				CoTraceInter.CoTraceSaveCsv.DataIdent := CoTraceInter.CoTraceRecorder.DataIdent;
				CoTraceInter.CoTraceSaveCsv.DecimalMark := '.';
							
				CoTraceInter.CoTraceSaveCsv.FileName := 'KUB';
				CoTraceInter.CoTraceSaveCsv.FileDevice := 'USER';
				CoTraceInter.CoTraceSaveCsv.Execute := TRUE;
				CoTraceInter.CoTraceSaveCsv();
				
				IF CoTraceInter.CoTraceSaveCsv.Done THEN
					CoTraceInter.CoTraceSaveCsv.Execute := FALSE;
					CoTraceInter.CoTraceSaveCsv();
				
					CoTraceInter.CoTraceRecorder();
					gCoTrace.Out.State.SM := CT_DONE;	
				
				ELSIF CoTraceInter.CoTraceSaveCsv.Error THEN
					gCoTrace.Out.State.SM := CT_ERROR;
				
				END_IF
		
		CT_DONE:
			gCoTrace.Out.Status.Done := TRUE;
			gCoTrace.Out.Status.Ready := TRUE;
			
			IF gCoTrace.Inp.Cmd.Start THEN
				gCoTrace.Out.State.SM := CT_WAIT_FOR_CMD;		
			END_IF
			
			
		CT_ERROR:
			gCoTrace.Out.Status.Error := TRUE;
			gCoTrace.Out.Status.Ready := FALSE;
			
			IF gCoTrace.Inp.Cmd.ErrorReset THEN
				gCoTrace.Inp.Cmd.ErrorReset := FALSE;
				
				CoTraceInter.CoTraceSaveCsv.Execute 		:= FALSE;
				CoTraceInter.CoTraceAddDataPoint.Execute 	:= FALSE;
				CoTraceInter.CoTraceAddStartTrigger.Execute := FALSE;
				CoTraceInter.CoTraceCreate.Enable			:= FALSE;
				CoTraceInter.CoTraceRecorder.Enable 		:= FALSE;
				CoTraceInter.CoTraceSetTiming.Execute 		:= FALSE;
				CoTraceInter.CoTraceOptional.Execute 		:= FALSE;
				
				CoTraceInter.CoTraceSaveCsv();
				CoTraceInter.CoTraceAddDataPoint();
				CoTraceInter.CoTraceAddStartTrigger();
				CoTraceInter.CoTraceCreate();
				CoTraceInter.CoTraceRecorder();
				CoTraceInter.CoTraceSetTiming();
				CoTraceInter.CoTraceOptional();

				gCoTrace.Out.Status.Ready := TRUE;
				gCoTrace.Out.Status.Error := FALSE;
				gCoTrace.Out.State.SM := CT_WAIT_FOR_CMD;
			END_IF
		

	END_CASE;
	
	
	CoTraceInter.CoTraceCreate();
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

