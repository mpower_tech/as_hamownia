
TYPE
	CoTraceInterType : 	STRUCT 
		CoTraceCreate : CoTraceConfigCreate;
		CoTraceSetTiming : CoTraceConfigSetTiming;
		CoTraceAddDataPoint : CoTraceConfigAddDataPoint;
		CoTraceRecorder : CoTraceRecorder;
		CoTraceSaveCsv : CoTraceDataSaveCsv;
		CoTraceAddStartTrigger : CoTraceConfigAddStartTrigger;
		CoTraceOptional : CoTraceConfigOptionalParameter;
	END_STRUCT;
END_TYPE
