
TYPE
	CoTraceType : 	STRUCT 
		Inp : CtInp;
		Out : CtOut;
	END_STRUCT;
	CtInp : 	STRUCT 
		Cmd : CtInpCmd;
		Params : CtInpParams;
	END_STRUCT;
	CtInpCmd : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		ErrorReset : BOOL;
	END_STRUCT;
	CtInpParams : 	STRUCT 
		ParamsName : ARRAY[0..9]OF STRING[80];
		NumOfParams : USINT;
		TimeRecording : REAL;
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	CtOut : 	STRUCT 
		State : CtOutState;
		Status : CtOutStatus;
	END_STRUCT;
	CtOutState : 	STRUCT 
		SM : SM_CT;
	END_STRUCT;
	CtOutStatus : 	STRUCT 
		Done : BOOL;
		Error : BOOL;
		Ready : BOOL;
	END_STRUCT;
	SM_CT : 
		(
		CT_WAIT_FOR_CMD,
		CT_CONFIG_CREATE,
		CT_SET_TIMING,
		CT_ADD_DATA_POINT,
		CT_SET_TRIGGER,
		CT_OPTIONAL,
		CT_RECORD,
		CT_SAVE_CSV,
		CT_DONE,
		CT_ERROR
		);
END_TYPE
