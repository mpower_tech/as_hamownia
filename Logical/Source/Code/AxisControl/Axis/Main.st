PROGRAM _INIT
	//ModbusTCPArray[18] :=	REAL_TO_UINT((9.55 * gExtDev.PowerMeter.torque / (MpAxisBasic_0.Velocity / 360 * 60))); // Pwr
	MpAxisBasic_0.Axis   	 := ADR(gAxis01);
	MpAxisBasic_0.Enable 	 := TRUE;
	MpAxisBasic_0.MpLink 	 := ADR(gAxisBasic);
	MpAxisBasic_0.Parameters := ADR(MpAxisBasicParTyp);
	
	MpAxisBasicParTyp.CyclicRead.TorqueMode := mpAXIS_READ_CYCLIC;
	MpAxisBasicParTyp.CyclicRead.LagErrorMode := mpAXIS_READ_CYCLIC;
	
	MpAxisBasicParTyp.Acceleration := 30000;
	MpAxisBasicParTyp.Deceleration := 5000;
	MpAxisBasicParTyp.Velocity     := 360;
	
	MpAxisCyclicSet_0.Enable 	 := TRUE;
	MpAxisCyclicSet_0.MpLink 	 := ADR(gAxisBasic);
	MpAxisCyclicSet_0.Parameters := ADR(MpAxisCyclicSetParTyp);
	
	MpAxisCyclicSetParTyp.Acceleration := 30000;
	MpAxisCyclicSetParTyp.Deceleration := 5000;
	MpAxisCyclicSetParTyp.TorqueSetup.RampedControl.PositiveMaxVelocity := 36000;
	MpAxisCyclicSetParTyp.TorqueSetup.RampedControl.NegativeMaxVelocity := 0;
	MpAxisCyclicSetParTyp.TorqueSetup.RampedControl.TorqueRamp := 0.2 ;
	MpAxisCyclicSetParTyp.TorqueSetup.Mode := mpAXIS_TORQUE_MODE_RAMPED_CTRL;
		
	ReadParams.Axis := ADR(gAxis01);
	ReadParams.Execute := TRUE;
	ReadParams.ParID := 214;
	ReadParams.DataAddress := ADR(DataVar.CurrentIq);
	ReadParams.DataType := ncPAR_TYP_REAL;
	
	ReadParams1.Axis := ADR(gAxis01);
	ReadParams1.Execute := TRUE;
	ReadParams1.ParID := 844;
	ReadParams1.DataAddress := ADR(DataVar.Power);
	ReadParams1.DataType := ncPAR_TYP_REAL;
	
	ReadParams2.Axis := ADR(gAxis01);
	ReadParams2.Execute := TRUE;
	ReadParams2.ParID := 219;
	ReadParams2.DataAddress := ADR(DataVar.CurrentId);
	ReadParams2.DataType := ncPAR_TYP_REAL;
	
	ReadParams3.Axis := ADR(gAxis01);
	ReadParams3.Execute := TRUE;
	ReadParams3.ParID := 298;
	ReadParams3.DataAddress := ADR(DataVar.DcBusVoltage);
	ReadParams3.DataType := ncPAR_TYP_REAL;
	
	yarray[0] := -200.0;
	yarray[1] := -160.0;
	yarray[2] := -120.0;
	yarray[3] := -80.0;
	yarray[4] := -40.0;
	yarray[5] :=  0.0;
	yarray[6] := 40.0;
	yarray[7] := 80.0;
	yarray[8] := 120.0;
	yarray[9] := 160.0;
	yarray[10]:= 200.0;

	xarray[0] := 0.0;
	xarray[1] := 1.0;
	xarray[2] := 2.0;
	xarray[3] := 3.0;
	xarray[4] := 4.0;
	xarray[5] := 5.0;
	xarray[6] := 6.0;
	xarray[7] := 7.0;
	xarray[8] := 8.0;
	xarray[9] := 9.0;
	xarray[10]:= 10.0;
	
	lookUpTable.Enable := TRUE;
	lookUpTable.NumberOfNodes := 11;
	lookUpTable.NodeVectorX := xarray;
	lookUpTable.FcnValues := yarray;
	lookUpTable.Mode := mtLOOKUP_LINEAR_EXTRAPOLATION;
	lookUpTable();

	MoveAverage.Enable := TRUE;
	MoveAverage.WindowLength := 64;
	
	MoveAverageUd.Enable := TRUE;
	MoveAverageUd.WindowLength := 64;
	
	MoveAverageUq.Enable := TRUE;
	MoveAverageUq.WindowLength := 64;
	
	MoveAverageTorque.Enable := TRUE;
	MoveAverageTorque.WindowLength := 64;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	IF MpAxisBasic_0.Error OR MpAxisCyclicSet_0.Error THEN
		
		MpAxisCyclicSet_0.CyclicPosition := FALSE;
		MpAxisCyclicSet_0.CyclicTorque := FALSE;
		MpAxisBasic_0.Power := FALSE;
		MpAxisBasic_0.Home  := FALSE;
		gAxis.Out.State.SM_AXIS := AXIS_ERROR;
	END_IF
	
	
	CASE gAxis.Out.State.SM_AXIS OF
		AXIS_WAIT_FOR_ACTIVATION:
			IF MpAxisBasic_0.Active THEN				
				MpAxisBasic_0.Home := TRUE;
				gAxis.Out.State.SM_AXIS := AXIS_WAIT_FOR_HOMING;

			END_IF
		
		AXIS_WAIT_FOR_HOMING:
			IF MpAxisBasic_0.IsHomed THEN
				MpAxisBasic_0.Home := FALSE;
				gAxis.Out.State.SM_AXIS := AXIS_WAIT_FOR_CMD;
				
			END_IF
				
		AXIS_WAIT_FOR_CMD:
			MpAxisBasic_0.Power := gAxis.Inp.Cmd.Enable;
			
			IF gAxis.Inp.Cmd.Move THEN
				IF gAxis.Inp.Cmd.Mode = 0 THEN
					MpAxisCyclicSet_0.CyclicVelocity := TRUE;
				
				ELSE
					MpAxisCyclicSet_0.CyclicTorque := TRUE;
				END_IF	
					
			ELSE
				MpAxisCyclicSet_0.CyclicVelocity := FALSE;
				MpAxisCyclicSet_0.CyclicTorque := FALSE;
			END_IF	
			
		AXIS_ERROR:
			
			IF gAxis.Inp.Cmd.ErrorReset THEN
				gAxis.Inp.Cmd.ErrorReset := FALSE;
				
				IF MpAxisBasic_0.Error THEN
					MpAxisBasic_0.ErrorReset := TRUE;
				END_IF
				
				IF MpAxisCyclicSet_0.Error THEN
					MpAxisCyclicSet_0.ErrorReset := TRUE;
				END_IF
				
			END_IF
		
			IF NOT gAxis.Out.Status.Error THEN
				MpAxisBasic_0.ErrorReset := FALSE;
				MpAxisCyclicSet_0.ErrorReset := FALSE;
				gAxis.Out.State.SM_AXIS := AXIS_WAIT_FOR_ACTIVATION;
			END_IF
		
	END_CASE;
	
	
	IF MpAxisBasic_0.Active THEN
		// Current	
		
		ReadParams();
		
		IF NOT ReadParams.Execute THEN
			ReadParams.Execute := TRUE;
		END_IF
		
		IF ReadParams.Done THEN
			ReadParams.Execute := FALSE;
			
			MoveAverageUq.In := DataVar.CurrentIq;
			MoveAverageUq();
			
			ModbusTCPArray[2] := REAL_TO_UINT(MoveAverageUq.Out*100); // Act Current
			
		END_IF
		
		ReadParams1();
		
		IF NOT ReadParams1.Execute THEN
			ReadParams1.Execute := TRUE;
		END_IF
		
		IF ReadParams1.Done THEN
			ReadParams1.Execute := FALSE;
			
	
			MoveAverage.In := DataVar.Power;
			MoveAverage();
			
			ModbusTCPArray[4] := REAL_TO_UINT(MoveAverage.Out *100); //(DataVar.Power Act Power
			
		END_IF
		
		ReadParams2();
		
		IF NOT ReadParams2.Execute THEN
			ReadParams2.Execute := TRUE;
		END_IF
		
		IF ReadParams2.Done THEN
			ReadParams2.Execute := FALSE;
			
			MoveAverageUd.In := DataVar.CurrentId;
			MoveAverageUd();
			
			ModbusTCPArray[1] := REAL_TO_UINT(MoveAverageUd.Out *100); // Act Current id
			
		END_IF
		
		ReadParams3();
		
		IF NOT ReadParams3.Execute THEN
			ReadParams3.Execute := TRUE;
		END_IF
		
		IF ReadParams3.Done THEN
			ReadParams3.Execute := FALSE;
			ModbusTCPArray[19] := REAL_TO_UINT(DataVar.DcBusVoltage); // Act Current id
			
		END_IF
		
	END_IF  
	
	gAxis.Out.Status.Error   := MpAxisBasic_0.Error OR MpAxisCyclicSet_0.Error;
	gAxis.Out.Status.IsRun   := MpAxisBasic_0.MoveActive;
	gAxis.Out.Status.PowerOn := MpAxisBasic_0.PowerOn; 
		
	ModbusTCPArray[0] := REAL_TO_UINT((MpAxisBasic_0.Velocity / 360 * 60)); // Act Velocity
//	ModbusTCPArray[1] := REAL_TO_UINT(0.0); // Act Frequency
	
	MoveAverageTorque.In := LREAL_TO_REAL(MpAxisBasic_0.Info.CyclicRead.Torque.Value);
	MoveAverageTorque();
	
	ModbusTCPArray[3] := LREAL_TO_UINT(MoveAverageTorque.Out*100); // Act Torque
//	ModbusTCPArray[4] := REAL_TO_UINT(0.0); // Act Power
	ModbusTCPArray[5] := REAL_TO_UINT(0.0); // Act DC Voltage
	ModbusTCPArray[6] := REAL_TO_UINT(0.0); // Act Main Voltage
	
	ModbusTCPArray[7] := REAL_TO_UINT(0.0); // Control Word 
	
	ModbusTCPArray[8].3 := gAxis.Out.Status.Error; // Status Word
	ModbusTCPArray[8].8 := gAxis.Out.Status.IsRun;
	ModbusTCPArray[8].0 := gAxis.Out.Status.PowerOn;
	
	ModbusTCPArray[9] := REAL_TO_UINT(0.0); // Aux Status Word	 
	MpAxisBasicParTyp.Torque.Limit := UINT_TO_REAL(ModbusTCPArray[10]) / 10.0;
	
	// Torque meter calculations
	gExtDev.PowerMeter.voltage  := INT_TO_REAL(gExtDev.PowerMeter.analogTorque)*10.0 / 32768.0;
	gExtDev.PowerMeter.velocity := gAxis.Out.Status.ActVelocity;
	
	lookUpTable.InX := gExtDev.PowerMeter.voltage;
	lookUpTable();
	gExtDev.PowerMeter.torque   := lookUpTable.Out;
	
	
	ModbusTCPArray[16] :=	REAL_TO_UINT(gExtDev.PowerMeter.torque * 100.0); // Tqr
	ModbusTCPArray[17] :=	ModbusTCPArray[0]*10; // Vel

	ModbusTCPArray[18] :=	REAL_TO_UINT(gExtDev.PowerMeter.torque * ((UINT_TO_REAL(ModbusTCPArray[17])/100)*2*3.1415)/60.0); // Pwr
	// Nm * rpm * 2 * PI / 600
	
	gAxis.Inp.Cmd.Enable := ModbusTCPArray[14].0; // Control word
	gAxis.Inp.Cmd.Move   := ModbusTCPArray[14].3;
	gAxis.Inp.Cmd.Mode   := ModbusTCPArray[14].11;
	gAxis.Inp.Cmd.ErrorReset := ModbusTCPArray[14].7;
	
	IF NOT ModbusTCPArray[11].15 THEN
		gAxis.Inp.Params.Velocity := UINT_TO_REAL(ModbusTCPArray[11]) / 10.0 * 360 / 60;
	ELSE
		temp2 := ModbusTCPArray[11];
		temp2.15 := 0;
		gAxis.Inp.Params.Velocity := (temp + temp2) / 10.0 * 360 / 60;
	END_IF
	
		
//	gAxis.Out.Status.Uq    := UINT_TO_REAL(ModbusTCPArray[16]) / 1000.0;
//	gAxis.Out.Status.Ud    := UINT_TO_REAL(ModbusTCPArray[17]) / 1000.0;

	IF NOT ModbusTCPArray[15].15 THEN
		gAxis.Out.Status.SetIq := UINT_TO_REAL(ModbusTCPArray[15]) / 1000.0;
	ELSE
		temp2 := ModbusTCPArray[15];
		temp2.15 := 0;
		gAxis.Out.Status.SetIq := (temp + temp2) / 1000.0;
		
	END_IF
	
	IF NOT ModbusTCPArray[16].15 THEN
		gAxis.Out.Status.Uq := UINT_TO_REAL(ModbusTCPArray[16]) / 1000.0;
	ELSE
		temp2 := ModbusTCPArray[16];
		temp2.15 := 0;
		gAxis.Out.Status.Uq := (temp + temp2) / 1000.0;
		
	END_IF
	
	
	IF NOT ModbusTCPArray[17].15 THEN
		gAxis.Out.Status.Ud := UINT_TO_REAL(ModbusTCPArray[17]) / 1000.0;
	ELSE
		temp2 := ModbusTCPArray[17];
		temp2.15 := 0;
		gAxis.Out.Status.Ud := (temp + temp2) / 1000.0;
		
	END_IF
	
	IF NOT ModbusTCPArray[12].15 THEN
		gAxis.Inp.Params.Torque := UINT_TO_REAL(ModbusTCPArray[12]) / 10.0;	
	ELSE
		temp2 := ModbusTCPArray[12];
		temp2.15 := 0;
		gAxis.Inp.Params.Torque := (temp + temp2) / 10.0;	
	END_IF
	
	
	
	gAxis.Inp.Params.TorqueLimit := UINT_TO_REAL(ModbusTCPArray[13]) / 100.0;	
	MpAxisCyclicSet_0.Velocity := REAL_TO_LREAL(gAxis.Inp.Params.Velocity); // 360 * 60
	MpAxisCyclicSet_0.Torque   := REAL_TO_LREAL(gAxis.Inp.Params.Torque);
	
	gAxis.Out.Status.ActVelocity := MpAxisBasic_0.Velocity / 360.0 * 60.0;
	gAxis.Out.Status.ActTorque   :=LREAL_TO_REAL(MpAxisBasic_0.Info.CyclicRead.Torque.Value);
	gAxis.Out.Status.ActPower    := MoveAverage.Out; //DataVar.Power;
	
		
	IF gAxis.Out.Status.TimeTheLastRequest > 3000 THEN
		MpAxisCyclicSet_0.CyclicVelocity := FALSE;
		MpAxisBasic_0.Power := FALSE;
		
	END_IF
	
	
	MpAxisBasic_0();
	MpAxisCyclicSet_0();
	
	
	
	
	 
END_PROGRAM