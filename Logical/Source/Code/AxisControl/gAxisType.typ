
TYPE
	gAxisType : 	STRUCT 
		Inp : gAxisInpTyp;
		Out : gAxisOutTyp;
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	gAxisInpTyp : 	STRUCT 
		Cmd : gAxisInpCmdTyp;
		Params : gAxisInpParamsTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	gAxisInpCmdTyp : 	STRUCT 
		Enable : BOOL;
		Mode : INT;
		Move : BOOL;
		ErrorReset : BOOL;
	END_STRUCT;
END_TYPE

(**)

TYPE
	gAxisInpParamsTyp : 	STRUCT 
		Torque : REAL;
		Velocity : REAL;
		TorqueLimit : REAL;
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	gAxisOutTyp : 	STRUCT 
		Status : gAxisOutStatusTyp;
		State : gAxisOutStateTyp;
	END_STRUCT;
END_TYPE

(**)

TYPE
	gAxisOutStatusTyp : 	STRUCT 
		PowerOn : BOOL;
		IsRun : BOOL;
		Error : BOOL;
		ActTorque : REAL;
		ActVelocity : REAL;
		ActCurrent : REAL;
		ActFreq : REAL;
		ActPower : REAL;
		SetIq : REAL;
		Uq : REAL;
		Ud : REAL;
		ComErr : BOOL;
		TimeTheLastRequest : DINT;
	END_STRUCT;
	gAxisOutStateTyp : 	STRUCT 
		SM_AXIS : SM_AXIS;
	END_STRUCT;
END_TYPE

(**)
(**)

TYPE
	SM_AXIS : 
		(
		AXIS_WAIT_FOR_ACTIVATION := 0,
		AXIS_WAIT_FOR_HOMING := 1,
		AXIS_WAIT_FOR_CMD := 2,
		AXIS_ERROR := 4
		);
END_TYPE
