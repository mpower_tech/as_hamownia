
TYPE
	gExtDevTyp : 	STRUCT 
		PowerMeter : gExtDevPowerMeter;
	END_STRUCT;
	gExtDevPowerMeter : 	STRUCT 
		analogTorque : INT;
		analogVelocity : INT;
		velocity : REAL;
		torque : REAL;
		voltage : REAL;
	END_STRUCT;
END_TYPE
